resource "azurerm_kubernetes_cluster" "aks_cluster" {
  for_each = var.environment
  depends_on = [
    azurerm_resource_group.rg
  ]
  name                = "${var.resource_group_name}-${each.key}-01"
  location            = each.value
  resource_group_name = "${var.resource_group_name}-${each.key}"
  dns_prefix          = "${var.resource_group_name}-${each.key}-cluster"
  #kubernetes_version  = data.azurerm_kubernetes_service_versions.current.latest_version
  kubernetes_version = "1.20.9"
  node_resource_group = "${var.resource_group_name}-${each.key}-nrg"

  default_node_pool {
    name                 = "systempool"
    vm_size              = "Standard_F2s_v2"
    #orchestrator_version = data.azurerm_kubernetes_service_versions.current.latest_version
    orchestrator_version = "1.20.9"
    node_count = 3
    os_disk_size_gb      = 30
    node_labels = {
      "nodepool-type"    = "system"
      "environment"      = "dev"
      "nodepoolos"       = "linux"
      "app"              = "system-apps" 
    } 
   tags = {
      "nodepool-type"    = "system"
      "environment"      = "dev"
      "nodepoolos"       = "linux"
      "app"              = "system-apps" 
   } 
  }
# Identity (System Assigned or Service Principal)
  identity {
    type = "SystemAssigned"
  }

# Network Profile
  network_profile {
    network_plugin = "azure"
    load_balancer_sku = "Standard"
  }

  tags = {
    Environment = "${each.key}"
  }
}
