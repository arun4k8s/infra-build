output "resource_group_Name" {
    value = { for val in azurerm_resource_group.rg: val.name => val.name }
}

output "location" {
    value = { for val in azurerm_resource_group.rg: val.location => val.location }
}
