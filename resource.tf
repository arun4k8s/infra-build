resource "azurerm_resource_group" "rg" {
    for_each = var.environment
    name = "${var.resource_group_name}-${each.key}"
    location = each.value

}