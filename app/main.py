from flask import Flask
import os
app = Flask(__name__)
@app.route("/")
def hello():
    pod_name = os.environ['MY_POD_NAME']
    node_name = os.environ['MY_NODE_NAME']
    strin = "Hello World K8S!!!\nMy Pod Name : {}\nMy node Name: {}\n".format(pod_name, node_name)
    return strin
if __name__ == "__main__":
    app.run(host='0.0.0.0')