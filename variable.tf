variable "resource_group_name" {
    default = "aks-cluster"
        type = string
}

variable "environment" {
    type = map(string)
    default = {
        "Prod" = "eastus"
        "DR" = "westus"
      }
}
  