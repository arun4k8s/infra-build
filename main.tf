terraform {
    required_version = ">= 0.13"

  required_providers {
      azurerm = {
          source = "hashicorp/azurerm"
          version = "~> 2.0" 
      }
    
  }

backend "azurerm" {
    resource_group_name   = "terraform-storage-rg"
    storage_account_name  = "terraformstate755"
    container_name        = "tfstatefiles"
    key                   = "dev1.terraform.tfstate"
  }  
}

provider "azurerm" {
    features {
      
    }
  
}



